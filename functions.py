def greet(first_name, last_name): #function parameters are inputs defined for a function
    print(f"Hi {first_name} {last_name} ")
    print("Are you understanding so far?")


greet("Tubo", "Nemi") #arguments are actual values for the function

greet("Dave", "Batista")
