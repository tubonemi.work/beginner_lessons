
bids = {}
bidding_finished = False


def find_winner(bids_record):
    highest_bid = 0
    winner = ""
    for bid in bids_record:
        amount = bids_record[bid]
        if amount > highest_bid:
            highest_bid = amount
            winner = bid
    print(f"The winner is {winner} with a bid of $ {highest_bid}")


while not bidding_finished:
    name = input("what is your name?: ")
    price = int(input("what is your bid?: $ "))
    bids[name] = price
    should_continue = input("Are there more bidders?: Type 'yes' or 'no'. ").lower()
    if should_continue == "no":
        bidding_finished = True
        find_winner(bids)
