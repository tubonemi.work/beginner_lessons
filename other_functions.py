# simple function
"""
def greet():
  print("this")
  print("is")
  print("SPARTA!!!!!")

greet()
"""


# function that allows for input
"""
def greet_with_name(name):
  print(f"Hello {name}")
  print("Did you watch the game last night?")

greet_with_name("Ibiso")


def greet_with(name, location):
  #print(f"Hello {name}.\nHow is it in {location} ?")
  print(f"Hello {name}")
  print(f"How is it in {location}")



greet_with("James","Windsor")

greet_with(location="lagos", name="ebuka")
"""
# exercise1


import math

def paint_calc(height, width, cover):
  x = (height*width)/cover
  soln = math.ceil(x)
  print(f"You'll need {soln} cans of paint.")

test_h = int(input("What is the height of the wall?: ")) # Height of wall (m)
test_w = int(input("What is the width of the wall?: ")) # Width of wall (m)
coverage = 5
paint_calc(height=test_h, width=test_w, cover=coverage)

"""

# exercise2 - prime number check

"""

"""
def prime_checker(number):
  counter = 0
  for a in range(2,100):
    if number % a == 0:
      counter += 1
  if counter > 1:
    print(f"{number} is not a prime number")
  else:
    print(f"{number} is a prime number")

n = int(input("Enter a number: "))
prime_checker(number=n)
"""
"""

# exercise 2 alternate solution
def prime_checker(number):
    is_prime = True
    for i in range(2, number):
        if number % i == 0:
            is_prime = False
    if is_prime:
        print("It's a prime number.")
    else:
        print("It's not a prime number.")


n = int(input("Enter a number: "))
prime_checker(number=n)

"""