
print("Welcome to the Nemi's tip calculator!")

bill = input("What was the total bill? : $")
bill_as_float = float(bill)

tipPercentage = input("What percentage tip would you like to give? : ")
tipPercentage_as_float = float(tipPercentage)

actualPercentage = tipPercentage_as_float/100

tipAmount = bill_as_float * actualPercentage
totalAmount = bill_as_float + tipAmount

splitCount = input("How many people to split the bill? : ")
splitCount_as_int = int(splitCount)

payment = round((totalAmount/splitCount_as_int), 2)
print(f"Each person should pay ${payment}")

