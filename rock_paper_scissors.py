import random


rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''


computer_choice = random.randint(0, 2)


user_choice = int(input("What do you choose? Type 0 for Rock, 1 for Paper or 2 for Scissors.\n"))

if user_choice > 2:
    print("Incorrect input. Try again")
elif user_choice == 0:
    if computer_choice == 0:
        print(f"{rock}\nComputer chose: \n {rock}\n It's a draw.")
    elif computer_choice == 1:
        print(f"{rock}\nComputer chose: \n {paper}\n You lose.")
    else:
        print(f"{rock}\nComputer chose: \n {scissors}\n You win.")
elif user_choice == 1:
    if computer_choice == 0:
        print(f"{paper}\nComputer chose: \n {rock}\n You win.")
    elif computer_choice == 1:
        print(f"{paper}\nComputer chose: \n {paper}\n It's a draw.")
    else:
        print(f"{paper}\nComputer chose: \n {scissors}\n You lose.")
else:
    if computer_choice == 0:
        print(f"{scissors}\nComputer chose: \n {rock}\n You lose.")
    elif computer_choice == 1:
        print(f"{scissors}\nComputer chose: \n {paper}\n You win.")
    else:
        print(f"{scissors}\nComputer chose: \n {scissors}\n It's a draw.")
