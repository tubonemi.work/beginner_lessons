def names_uno(f_name, l_name):
    """This function takes a first name and last name then formats and returns them in Title case """
    formatted_firstname = f_name.title()
    formatted_lastname = l_name.title()
    full_name = f"{formatted_firstname} {formatted_lastname}"
    return full_name


print(names_uno("ryan", "henley"))


def names_dos(f_name, l_name):
    if f_name == "" or l_name == "":
        return "User did not enter any names"

    formatted_first_name = f_name.title()
    formatted_last_name = l_name.title()
    complete_name = f"{formatted_first_name} {formatted_last_name}"
    return complete_name


print(names_dos(l_name=input("Enter your last name:"), f_name=input("Enter your first name: ")))
