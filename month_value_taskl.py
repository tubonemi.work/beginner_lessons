def is_leap(year):
    """This function checks if the year is a leap year and returns true or false"""
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return True
            else:
                return False
        else:
            return True
    else:
        return False


def days_in_month(year, month_value):
    leap_value = is_leap(year)
    month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    if month_value > 12:
        return "Invalid month"
    elif leap_value is True and month_value == 2:
        return year, 29
    elif leap_value is False:
        month_value = month_days[month_value - 1]
        return year, month_value
    else:
        return "Invalid inputs!"


# 🚨 Do NOT change any of the code below
year = int(input())  # Enter a year
month = int(input())  # Enter a month
days = days_in_month(year, month)
print(days)
